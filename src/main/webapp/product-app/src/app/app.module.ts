import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddMobileComponent } from './add-mobile/add-mobile.component';
import { MobileDetailsComponent } from './mobile-details/mobile-details.component';
import { MobilesComponent } from './mobiles/mobiles.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddMobileComponent,
    MobileDetailsComponent,
    MobilesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
