import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

import { Mobile } from './mobile';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  apiUrl='http://localhost:8080/mobile/';
  
  httpOptions={headers:new HttpHeaders({
  'Content-Type': 'application/json'
  })}
  constructor(private http: HttpClient) { }
  
  getMobile(id : number){
  	const getMobileUrl=this.apiUrl+'get/'+id;
  	return this.http.get<Mobile>(getMobileUrl);
  }
  
  getAllMobile(){
  	const getAllUrl=this.apiUrl+'getall';
  	return this.http.get<Array<Mobile>>(getAllUrl);
  }
  
  addMobile(mobile : Mobile){
  	const addUrl=this.apiUrl+'add';
  	return this.http.post<Mobile>(addUrl, mobile,this.httpOptions);
  }
  
  deleteMobile(id : number) {
  	const deleteUrl=this.apiUrl+'delete/'+id;
  	return this.http.delete<Mobile>(deleteUrl);
  }
}
