import { Component, OnInit } from '@angular/core';

import { Mobile } from '../mobile';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-add-mobile',
  templateUrl: './add-mobile.component.html',
  styleUrls: ['./add-mobile.component.css']
})
export class AddMobileComponent implements OnInit {
	
  submitted = false;
  mobile : Mobile;
  
  constructor(private productService : ProductService) { }

  ngOnInit(): void {
  this.mobile = new  Mobile();
  }

  addMobile() {
 	this.productService.addMobile(this.mobile).subscribe(response => {
 	this.submitted = true;
 	});
  }

}
