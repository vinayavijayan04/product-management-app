export class Mobile{
	mobileId : number;
	mobileName : string;
	mobileMake : string;
	mobilePrice : number;
	mobileReleaseYear : number;
	mobileDesc : string;
	
}