import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AddMobileComponent } from './add-mobile/add-mobile.component';
import { MobileDetailsComponent } from './mobile-details/mobile-details.component';
import { MobilesComponent } from './mobiles/mobiles.component';

const routes: Routes = [
  		{path: 'home',component: HomeComponent},
  		{path: 'add',component: AddMobileComponent},
  		{path: 'details/:id',component: MobileDetailsComponent},
  		{path: 'mobiles',component: MobilesComponent},
  		{ path: '', redirectTo: 'home', pathMatch: 'full' }
  		];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
