import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Mobile } from '../mobile';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-mobile-details',
  templateUrl: './mobile-details.component.html',
  styleUrls: ['./mobile-details.component.css']
})
export class MobileDetailsComponent implements OnInit {
 
  mobile : Mobile;
 	
  constructor(private route: ActivatedRoute,
    private productService : ProductService,
    private location: Location) { }
  
  ngOnInit(): void {
  	const id = this.route.snapshot.params['id'];
  	this.getMobile(id);
  }


   getMobile(id : number): void {
    
    this.productService.getMobile(id).subscribe(response => {
      this.mobile = response;
      });
  	}

  goBack(): void {
    this.location.back();
  }
}
