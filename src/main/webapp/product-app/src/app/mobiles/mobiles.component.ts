import { Component, OnInit } from '@angular/core';

import { Mobile } from '../mobile';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-mobiles',
  templateUrl: './mobiles.component.html',
  styleUrls: ['./mobiles.component.css']
})
export class MobilesComponent implements OnInit {

  mobiles : Array<Mobile>;
	
  constructor(private productService : ProductService) { }

  ngOnInit(): void {
  	this.getAllMobile();
  }
  
  getAllMobile(){
  this.productService.getAllMobile().subscribe(response => {
  this.mobiles = response;
  });
  }
  
  deleteMobile(id : number) {
  	this.productService.deleteMobile(id).subscribe(response =>{
  		this.ngOnInit();
  	});
  }	
}
