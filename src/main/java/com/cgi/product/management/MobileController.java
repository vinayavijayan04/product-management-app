/**
 * 
 */
package com.cgi.product.management;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author vinay
 *
 */
@RestController
@RequestMapping(path = "/mobile")
@CrossOrigin(allowedHeaders = "*")
public class MobileController {
	@Autowired
	private MobileService mobileService;

	@GetMapping(path = "/get/{mobileId}")
	public MobileEntity getMobile(@PathVariable int mobileId) {
		return mobileService.getMobile(mobileId);
	}

	@GetMapping(path = "/getall")
	public Iterable<MobileEntity> getAllMobile() {
		return mobileService.getAllMobile();
	}

	@PostMapping(path = "/add")
	public MobileEntity addMobile(@RequestBody MobileEntity mobileEntity) {
		return mobileService.addMobile(mobileEntity);
	}

	@DeleteMapping(path = "/delete/{mobileId}")
	public void deleteMobile(@PathVariable int mobileId) {
		mobileService.deleteMobile(mobileId);
	}
}
