/**
 * 
 */
package com.cgi.product.management;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vinay
 *
 */
@Repository
public interface MobileRepository extends CrudRepository<MobileEntity, Integer>{

}
