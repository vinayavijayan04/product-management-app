/**
 * 
 */
package com.cgi.product.management;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author vinay
 *
 */
@Entity
@Table(name = "mobile")
public class MobileEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mobile_id")
	private Integer mobileId;
	@Column(name = "mobile_name")
	private String mobileName;
	@Column(name = "mobile_make")
	private String mobileMake;
	@Column(name = "mobile_price")
	private double mobilePrice;
	@Column(name = "mobile_release_year")
	private int mobileReleaseYear;
	@Column(name = "mobile_desc")
	private String mobileDesc;

	/**
	 * @return the mobileId
	 */
	public Integer getMobileId() {
		return mobileId;
	}

	/**
	 * @param mobileId the mobileId to set
	 */
	public void setMobileId(Integer mobileId) {
		this.mobileId = mobileId;
	}

	/**
	 * @return the mobileName
	 */
	public String getMobileName() {
		return mobileName;
	}

	/**
	 * @param mobileName the mobileName to set
	 */
	public void setMobileName(String mobileName) {
		this.mobileName = mobileName;
	}

	/**
	 * @return the mobileMake
	 */
	public String getMobileMake() {
		return mobileMake;
	}

	/**
	 * @param mobileMake the mobileMake to set
	 */
	public void setMobileMake(String mobileMake) {
		this.mobileMake = mobileMake;
	}

	/**
	 * @return the mobilePrice
	 */
	public double getMobilePrice() {
		return mobilePrice;
	}

	/**
	 * @param mobilePrice the mobilePrice to set
	 */
	public void setMobilePrice(double mobilePrice) {
		this.mobilePrice = mobilePrice;
	}

	/**
	 * @return the mobileReleaseYear
	 */
	public int getMobileReleaseYear() {
		return mobileReleaseYear;
	}

	/**
	 * @param mobileReleaseYear the mobileReleaseYear to set
	 */
	public void setMobileReleaseYear(int mobileReleaseYear) {
		this.mobileReleaseYear = mobileReleaseYear;
	}

	/**
	 * @return the mobileDesc
	 */
	public String getMobileDesc() {
		return mobileDesc;
	}

	/**
	 * @param mobileDesc the mobileDesc to set
	 */
	public void setMobileDesc(String mobileDesc) {
		this.mobileDesc = mobileDesc;
	}

}
