/**
 * 
 */
package com.cgi.product.management;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author vinay
 *
 */
@Service
public class MobileService {

	@Autowired
	private MobileRepository mobileRepository;

	public MobileEntity getMobile(int mobileId) {
		Optional<MobileEntity> mobileDetails = mobileRepository.findById(mobileId);
		if (mobileDetails.isPresent()) {
			return mobileDetails.get();
		} else {
			throw new RuntimeException("Mobile not found for the id : " + mobileId);
		}
	}

	public Iterable<MobileEntity> getAllMobile() {
		return mobileRepository.findAll();

	}

	public MobileEntity addMobile(MobileEntity mobileEntity) {
		return mobileRepository.save(mobileEntity);
	}

	public void deleteMobile(int mobileId) {
		mobileRepository.deleteById(mobileId);
	}
}
